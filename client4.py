import sys
import requests
from datetime import datetime
from PyQt5 import QtWidgets
from PyQt5.QtWidgets import *
from PyQt5.QtGui import *
from PyQt5.QtCore import *
from PyQt5 import *
import json
from akk import Ui_MainWindow  # импорт нашего сгенерированного файла


class MyWindows(QtWidgets.QMainWindow):
    value_1 = 0

    def __init__(self):

        super(MyWindows, self).__init__()
        self.ui = Ui_MainWindow()
        self.ui.setupUi(self)

        datetime_element = datetime.strftime(datetime.now(), "%Y.%m.%d %H:%M:%S")

        self.ui.pushButton_2.setText(datetime_element)
        self.ui.pushButton_2.clicked.connect(self.check_time)

        result_for_server = requests.get('http://127.0.0.1:5000/log')

        self.ui.label_2.setText(str(result_for_server.text))
        self.ui.pushButton.clicked.connect(self.load_sms)

    def load_sms(self):
        result_for_server = requests.get('http://127.0.0.1:5000/sms')
        ty = result_for_server.json()
        elements_zero = len(ty)

        if elements_zero == 0:
            pass
        else:
            for values in ty[0]:
                Any1 = values

            er = ty[0].get(Any1)
            self.ui.label_10.setText(er)

            self.ui.pushButton_3.clicked.connect(self.view_sms)
            self.ui.pushButton_4.clicked.connect(self.paging_sms)

    def view_sms(self):
        Response = requests.get('http://127.0.0.1:5000/sms')
        ty = Response.json()

        if self.value_1 != (len(ty)):

            for values in ty[0 + self.value_1]:
                Any = values
            er = ty[0 + self.value_1].get(Any)

            self.ui.label_10.setText(er)
            self.value_1 += 1
        else:
            pass

    def paging_sms(self):
        Response = requests.get('http://127.0.0.1:5000/sms')
        ty = Response.json()
        if self.value_1 != 0:
            self.value_1 -= 1
            for values in ty[0 + self.value_1]:
                element = values
            result = ty[0 + self.value_1].get(element)

            self.ui.label_10.setText(result)
            print(self.value_1, end="")
        else:
            pass

    def check_time(self):
        self.ui.pushButton_2.setText(datetime.strftime(datetime.now(), "%Y.%m.%d %H:%M:%S"))


app = QtWidgets.QApplication([])
application = MyWindows()
application.show()
