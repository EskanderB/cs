import sys
import requests
from PyQt5 import QtWidgets
from PyQt5.QtWidgets import *
from PyQt5.QtGui import *
from PyQt5.QtCore import *
from PyQt5 import *
import sqlite3 as lite
import json
from reg import U_MainWindow


class Mswindows(QtWidgets.QMainWindow):
    def __init__(self):
        super(Mswindows, self).__init__()
        self.ui = U_MainWindow()
        self.ui.setupUi(self)

        self.ui.lineEdit_3.clicked.connect(self.ui.lineEdit_3.clear)
        self.ui.lineEdit_4.clicked.connect(self.ui.lineEdit_4.clear)

        self.ui.pushButton.clicked.connect(self.account_registration)

        self.ui.lineEdit_3.clicked.connect(self.change_text_line)
        self.ui.lineEdit_4.clicked.connect(self.change_text_line_2)

    def change_text_line_2(self):
        self.ui.lineEdit_4.clear()
        self.ui.lineEdit_4.setStyleSheet("background-color:white")

    def change_text_line(self):
        self.ui.lineEdit_3.clear()
        self.ui.lineEdit_3.setStyleSheet("background-color:white")

    def account_registration(self):
        a = (self.ui.lineEdit_3.text())
        b = (self.ui.lineEdit_4.text())
        requests.get('http://127.0.0.1:5000/reg/' + a + '/' + b)


app = QtWidgets.QApplication([])
application = Mswindows()
application.show()
