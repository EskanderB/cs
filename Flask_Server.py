import json
import sqlite3 as lite

from flask import Flask
from flask import jsonify

a = lite.connect('Regis.db', check_same_thread=False)
app = Flask(__name__)


@app.route('/reg/<int:log>/<string:log1>')
def a1(log, log1):
    with a:
        cur = a.cursor()
        cur.execute("INSERT INTO Serve VALUES(?,?)", (log, log1))


value_1 = ""
value_2 = ""


@app.route('/in/<int:f>/<string:b>')
def a2(f, b):
    global value_1
    global value_2

    value_1 = f
    value_2 = b
    elements_from_table = a.execute("SELECT * FROM Serve").fetchall()

    for i in range(len(list(elements_from_table))):

        elements_table = list(elements_from_table[i])

        element_table_1 = f == elements_table[0]
        element_table_2 = b == elements_table[1]

        if element_table_1 and element_table_2:
            result = str(f)

            with open('eskaj2.json', 'w') as fcv:
                json.dump(result, fcv)

            return result


@app.route('/log', methods=['GET'])
def a3():
    print(value_1, value_2)
    result = a2(value_1, value_2)
    print(result)
    return result


@app.route('/sms', methods=['GET'])
def a4():

    with open('eskaj2.json') as fc:
        cv1 = fc.read()
        templates1 = json.loads(cv1)

    gh = templates1
    element_dictionary = {}

    list_of_items = []
    table_elements = a.execute("SELECT * FROM Client").fetchall()

    for i in range(len(table_elements)):
        list_table_elements = list(table_elements[i])
        cv = int(gh) == list_table_elements[0]

        if cv:
            element_dictionary[list_table_elements[0]] = list_table_elements[1]
            list_of_items.append(element_dictionary)
            element_dictionary = {}

    return jsonify(list_of_items)


if __name__ == "__main__":
    app.run(debug=True)
